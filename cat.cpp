///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file cat.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author @Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   22_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <cassert>
#include <random>
#include <queue>

#define CAT_NAMES_FILE "names.txt"

#include "cat.hpp"

using namespace std;

const int nameLen = 6;

Cat::Cat( const std::string newName ) {
	setName( newName );
}


void Cat::setName( const std::string newName ) {
	assert( newName.length() > 0 );

	name = newName;
}

string Cat::getName() {
   return name;
}

std::vector<std::string> Cat::names;


void Cat::initNames() {
	names.clear();
	cout << "Loading... ";

	ifstream file( CAT_NAMES_FILE );
	string line;
	while (getline(file, line)) names.push_back(line);

	cout << to_string( names.size() ) << " names." << endl;
}


Cat* Cat::makeCat() {
	// Get a random cat from names
	auto nameIndex = rand() % names.size();
	auto name = names[nameIndex];

	// Remove the cat (by index) from names
	erase( names, names[nameIndex] );

	return new Cat( name );
}

bool CatEmpire::empty() {
   if(topCat == nullptr)
      return true;
   
   return false;
}

void CatEmpire::addCat(Cat* newCat) {
   if( topCat == nullptr ) {
      topCat = newCat;
      return;
   }
   addCat( topCat, newCat);
}

void CatEmpire::catFamilyTree() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorderReverse( topCat, 1 );
}



void CatEmpire::catList() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsInorder( topCat );
}

void CatEmpire::catGenerations() const {
   if( topCat == nullptr ) {
      cout << "No cats!" << endl;
      return;
   }

   bfs( topCat);
   cout << endl;
}



void CatEmpire::catBegat() const {
	if( topCat == nullptr ) {
		cout << "No cats!" << endl;
		return;
	}

	dfsPreorder( topCat );
}

//private functions
void CatEmpire::addCat( Cat* atCat, Cat* newCat ) {
   if( atCat->name > newCat->name ){
      if( atCat->left == nullptr ){
         atCat->left = newCat;
      }
      else {
         addCat( atCat->left, newCat );
      }
   }
   else if( atCat->name < newCat->name ) {
      if(atCat->right == nullptr ){
         atCat->right = newCat;
      }
      else {
         addCat( atCat->right, newCat );
      }
   }
   else {
      cout << "invalid addition: Cats have same name" << endl;
      return;
   }
}

void CatEmpire::dfsInorderReverse( Cat* atCat, int depth ) const{
   if(atCat == nullptr){
      return;
   }
   depth++;
   dfsInorderReverse(atCat->right, depth);
   cout << string( nameLen * (depth-1), ' ' ) << atCat->name;
   if(atCat->left != nullptr && atCat->right != nullptr){//left and right cat
      cout << "<" << endl;
   }
   else if(atCat->left != nullptr && atCat->right == nullptr) {//no right cat
      cout << "\\" << endl;
   }
   else if(atCat->left == nullptr && atCat->right != nullptr) {//no left cat
      cout << "/" << endl;
   }
   else{//neither left or right cat
      cout << endl;
   }
   dfsInorderReverse(atCat->left, depth);
}

void CatEmpire::dfsInorder( Cat* atCat) const{
   if(atCat == nullptr){
      return;
   }
   dfsInorder(atCat->left);
   cout << atCat->name << endl;
   dfsInorder(atCat->right);
}

void CatEmpire::dfsPreorder( Cat* atCat) const{
   if(atCat == nullptr){
      return;
   }

   if(atCat->left != nullptr && atCat->right != nullptr){//left and right cat
      cout << atCat->name << " begat " << atCat->left->name << " and " << atCat->right->name << endl;
   }
   else if(atCat->left != nullptr && atCat->right == nullptr) {//no right cat
      cout << atCat->name << " begat " << atCat->left->name << endl;
   }
   else if(atCat->left == nullptr && atCat->right != nullptr) {//no left cat
      cout << atCat->name << " begat " << atCat->right->name << endl;
   }

   dfsPreorder(atCat->left);

   dfsPreorder(atCat->right);
}

void CatEmpire::getEnglishSuffix( int n ) const{
   int nth = n;
   //check if n is between 10-20
   if( n >= 10 && n <= 20){
      cout << n << "th";
   }
   else{//general case
      while(nth > 10){//separate out ones place digit
         nth -= 10;
      }

      if( nth == 1){
         cout << n << "st";
      }
      else if( nth == 2) {
         cout << n << "nd";
      }
      else if( nth == 3){
         cout << n << "rd";
      }
      else{
         cout << n << "th";
      }
   }
}

void CatEmpire::bfs(Cat* root) const{
   queue<Cat*> catQueue;
   catQueue.push(root);
   int depth = 1;
   Cat* leftMostCat = nullptr;
   bool isOriginalLeftMost = true;
   //get first left most cat
   if(root->left != nullptr){
      leftMostCat = root->left;
   }
   else if(root->right != nullptr){
      leftMostCat = root->right;
   }
   else{
      cout << "only 1 or zero cats" << endl;
      return;
   }
   
   getEnglishSuffix(depth);
   cout << " Generation" << endl;

   while(!catQueue.empty()){
      Cat* cat = catQueue.front();
      catQueue.pop();

      if(cat == nullptr){
         return;
      }
      
      if(cat == leftMostCat){//change depths if cat is leftMost
         depth++;
         if(isOriginalLeftMost == true){
            cout << endl;
            getEnglishSuffix(depth);
            cout << " Generation" << endl;
         }

         if(cat->left != nullptr){
            leftMostCat = cat->left;
            isOriginalLeftMost = true;
         }
         else if(cat->right != nullptr){
            leftMostCat = cat->right;
            isOriginalLeftMost = true;
         }
         else
         {
            leftMostCat = catQueue.front();
            isOriginalLeftMost = false;
            depth--;
         }
      }

      cout << "  " << cat->name;

      if(cat->left != nullptr){
         catQueue.push(cat->left);
      }
      if(cat->right != nullptr){
         catQueue.push(cat->right);
      }
   }
}
