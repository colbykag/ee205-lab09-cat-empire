///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file test.cpp
/// @version 1.0
///
/// test code and stuff
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   22_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <queue>
#include "cat.hpp"

using namespace std;

const int NUMBER_OF_CATS = 40;

int main() {
	cout << "Welcome to Cat Empire!" << endl;

	Cat::initNames();

	 CatEmpire catEmpire;

    //make tree of 30 cats
	for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();
      cout << newCat->getName() << endl;
		catEmpire.addCat( newCat );
	}

   queue<int> stuffs;
   stuffs.push(1);
   
	cout << "Print an alphabetized list of " << NUMBER_OF_CATS << " cats" << endl;

} // main()
